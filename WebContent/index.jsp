<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SEO Project Management System - SeoPMS</title>
</head>
<body>
	<header>
		<h1>SeoPMS</h1>
		<nav>
			<a href=".">Main page</a> |
			<a href="projects">Projects</a> |
			<a href="employees">Employees</a>
		</nav>
	</header>
	<article>
		<p>Welcome to the SEO Project Management System!</p>
		
	</article>
	<footer>
		<p>Author - Andrey Glyatsevich (paction[@]bk.ru)</p>
	</footer>
</body>
</html>